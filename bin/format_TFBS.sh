file=$1
annot_file=$2
output_dir=$3

file_bname=$(basename $file)
mat_parts=(${file_bname//"."/" "})
mat_base_id=${mat_parts[0]}
mat_version=${mat_parts[1]}

mat_id=${mat_base_id}"."${mat_version}

tf_name=$(grep "${mat_id}" $annot_file | cut -f 2)

# echo $file
# echo $tf_name

mkdir -p $output_dir/${mat_id}/

gunzip -c $file > ${output_dir}/${mat_id}/${mat_id}.bed
awk -v OFS='\t' '{print $1,$2,$3,$4,$5,$6}' ${output_dir}/${mat_id}/${mat_id}.bed \
> ${output_dir}/${mat_id}/${mat_id}.bed.tmp

mv ${output_dir}/${mat_id}/${mat_id}.bed.tmp ${output_dir}/${mat_id}/${mat_id}.bed

db_version=$(basename $annot_file)
db_version=${db_version//".tsv"/""}

printf "filename\tdescription\tdataSource\n${mat_id}.bed\t${tf_name}\t${db_version}\n" \
> ${output_dir}/${mat_id}/index.txt
