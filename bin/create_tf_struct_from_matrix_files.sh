DIR=$( realpath "$( dirname "$0" )" )

usage() {
  echo "Usage: $0 -i INPUT DIR -a ANNOT FILE -o OUTPUT DIR" 1>&2
}

while getopts "i:a:o:h" options; do

  case "${options}" in
    i)
      input_dir=${OPTARG}
      ;;
    a)
      annot_file=${OPTARG}
      ;;
    o)
      output_dir=${OPTARG}
      ;;
    h)
      usage
      echo ""
      echo "Prepare the data for the creation of the LOLA databases."
      echo ""
      echo "Arguments:"
      echo ""
      echo "    -i INPUT DIR: path to the input directory containing the downloaded files."
      echo "    -a ANNOT FILE: path to the .tsv annotation file created with the query_API.R script."
      echo "    -o OUTPUT DIR: path to the output directory."
      echo ""
      exit 0
      ;;
  esac

done

if [ ! -d $input_dir ]
then

  echo "Input directory does not exist! Exiting..."
  exit 1

fi

if [ ! -f $annot_file ]
then

  echo "Annotation file does not exist! Exiting..."
  exit 1

fi

if [ ! -d $output_dir ]
then

  mkdir -p $output_dir

fi

find $input_dir -name "*.gz" \
| xargs -I {} --max-procs=50 bash ${DIR}/format_TFBS.sh {} $annot_file $output_dir

max_TFs_per_batch=50

Rscript $DIR/split_tf_struct_into_batches.R \
-i $output_dir -b $max_TFs_per_batch -o $output_dir
