DIR=$( realpath "$( dirname "$0" )" )

usage() {
  echo "Usage: $0 -i INPUT TXT -o OUTPUT DIR" 1>&2
}

while getopts "i:o:h" options; do

  case "${options}" in
    i)
      input_file=${OPTARG}
      ;;
    o)
      output_dir=${OPTARG}
      ;;
    h)
      usage
      echo ""
      echo "Download TFBS files from a list of links in a TXT."
      echo ""
      echo "Arguments:"
      echo ""
      echo "    -i INPUT TXT: path to the input .txt file containing one link per line."
      echo "    -o OUTPUT DIR: path to the output directory."
      echo ""
      exit 0
      ;;
  esac

done

if [ ! -f $input_file ]
then

  echo "Input file does not exist! Exiting..."
  exit 1

fi

if [ ! -d $output_dir ]
then

  mkdir -p $output_dir

fi

cat $input_file \
| parallel -j 20 wget {} \
-P $output_dir
