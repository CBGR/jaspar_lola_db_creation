DIR=$( realpath "$( dirname "$0" )" )

usage() {
  echo "Usage: $0 -i INPUT SQL -o OUTPUT DIR" 1>&2
}

while getopts "i:o:h" options; do

  case "${options}" in
    i)
      input_sql=${OPTARG}
      ;;
    o)
      output_dir=${OPTARG}
      ;;
    h)
      usage
      echo ""
      echo "Extract the necessary data from the JASPAR SQL table."
      echo ""
      echo "Arguments:"
      echo ""
      echo "    -i INPUT SQL: path to the input .sqlite file containing one link per line."
      echo "    -o OUTPUT DIR: path to the output directory."
      echo ""
      exit 0
      ;;
  esac

done

if [ ! -f $input_sql ]
then

  echo "Input file does not exist! Exiting..."
  exit 1

fi

if [ ! -d $output_dir ]
then

  mkdir -p $output_dir

fi



sqlite3 $input_sql \
"SELECT BASE_ID,VERSION,NAME FROM MATRIX LEFT OUTER JOIN MATRIX_ANNOTATION ON MATRIX.ID = MATRIX_ANNOTATION.ID;" \
| awk -F"|" -v OFS='\t' '{print $1"."$2,$3}' \
| sed -i 's/(var./_var_/g' \
| awk -F"\t" -v OFS='\t' '{gsub("\\(", "_", $2); print}' \
| awk -F"\t" -v OFS='\t' '{gsub("\\)", "", $2); print}' \
> ${output_dir}/jaspar.tsv
