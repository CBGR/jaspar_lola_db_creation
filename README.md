# LOLA database creation for JASPAR TFBSs

This repository contains the necessary scripts to build a LOLA database for a set of JASPAR TFBSs. Because of the size of the TFBS files, it separates the LOLA databases into batches of max 50 TFs each. These batches can be used to compute the enrichment of different TFs in a set of regions. The enrichment tool in this [link](https://bitbucket.org/CBGR/jaspar_enrichment/) is tailored to use the databases created by the scripts in this repository and provide an aggregated result of the enrichment results.

## Dependencies

The pipeline has the following dependencies:

* **Python 3:** the last tested version is python 3.6.12.
* **Snakemake:** the last tested version is 5.5.4.
* **R:** the last tested version is R 4.0.2.
    * Optparse 1.6.6.
		* LOLA 1.18.1.
		* jsonlite 1.7.1.

## Downloading the pipeline

The source code to run this pipeline can be downloaded with:

```
git clone https://bitbucket.org/CBGR/jaspar_lola_db_creation.git
```

## Configuring the pipeline

Before starting, make sure you configure the pipeline correctly by editing the `config.yaml` file. Specifically, make sure you edit the `DATA_DIR` and `RESULTS` variables.

* `DATA_DIR`: this variable contains the path to the input data directory. **Data in this directory is expected to follow the following structure:**  
```
├── data
│   └── jasparVersion (e.g. JASPAR2020)
│       ├── genome (e.g. hg38)
│       │   ├── Matrices
│       │   │   └── Matrix_ID.tsv.gz (e.g. MA0002.2.tsv.gz)
│       │   └── TFs
│       │   |   ├── Batch_x (e.g. Batch_1)
│       │   |   │   ├── Matrix_ID (e.g. MA0002.2)
│       │   |   │   │   ├── Matrix_ID.bed (e.g. MA0002.2.bed)
│       │   |   │   │   └── index.txt
│       │   └── collection.txt (e.g. collection.txt)
│       └── jaspar.tsv
```
See the section named **Data preparation** to see how to generate this structure using some of the scripts in this repository.  
* `RESULTS`: this variable contains the path to the directory where you want to store the results. The output directory structure will be:
```
├── results
│   └── jasparVersion (e.g. JASPAR2020)
│       ├── genome (e.g. hg38)
│       │   ├── data
|       │   |   ├── Batch_x (e.g. Batch_1)
│       │   |   │   ├── Matrix_ID (e.g. MA0002.2)
│       │   |   │   │   ├── collection.txt
│       │   |   │   │   ├── index.txt
│       │   |   │   │   ├── sizes.txt
│       │   |   │   │   ├── regions
│       │   |   │   │   |   └── Matrix_ID.bed (e.g. MA0002.2.bed)
│       │   └── LOLA_dbs
│       │   |   └── JASPARVERSION_LOLA_Batch_x.RDS (e.g. JASPAR2020_LOLA_Batch_1.RDS)
```

## Data preparation

This section describes how to prepare the input data directory in order to run the processing pipeline. The initial required input is a text file where each line contains a link to a `.gz` compressed file with the TFBSs for a JASPAR profile (see file in `example_files/links.txt` for an example containing the links to TFBSs for _S. cerevisiae_ in JASPAR2020). With this, the sequence of steps to prepare the data is:

1. Download the TFBS files in `DATA_DIR/JASPAR_VERSION/GENOME_ASSEMBLY/Matrices/`, where `DATA_DIR` is the path to the input data directory set in the `config.yaml` file.
```
bash bin/download_mats.sh -i /path/to/links.txt -o DATA_DIR/JASPAR_VERSION/GENOME_ASSEMBLY/Matrices/
```
2. Obtain the metadata annotations from the JASPAR API:
```
Rscript bin/query_API.R -U MATRIX_API_URL -V JASPAR_RELEASE -o DATA_DIR/JASPAR_VERSION/
```
where:  
    * `MATRIX_API_URL` is the URL to the matrix API in JASPAR (typically, this will be http://jaspar.genereg.net/api/v1/matrix/, although the testing site API URL can also be used).  
    * `JASPAR_RELEASE` is an integer describing the JASPAR release to get the metadata annotations from (e.g. 2020 for JASPAR 2020).  
    * `DATA_DIR/JASPAR_VERSION/` is the path to the directory that will contain the data for a specific JASPAR release.
The output of this step will be a two column tab-separated file located at `DATA_DIR/JASPAR_VERSION/jaspar.tsv` containing a mapping between each JASPAR matrix ID and its TF name.
3. Extract the data in the downloaded matrices, convert to 1-based coordinates, separate by batches, and prepare the file structure expected by the snakemake pipeline.
```
bash bin/create_tf_struct_from_matrix_files.sh \
-i DATA_DIR/JASPAR_VERSION/GENOME_ASSEMBLY/Matrices/ \
-a DATA_DIR/JASPAR_VERSION/jaspar.tsv \
-o DATA_DIR/JASPAR_VERSION/GENOME_ASSEMBLY/TFs/
```

Once having followed these steps, the data will be ready for the snakemake pipeline.

## Running the pipeline

After preparing the data following the instructions in the **Data preparation** section, run the processing pipeline with:
```
snakemake --cores N_CORES
```
where N_CORES is the number of cores to use in parallel computing.  
:warning: **Note! Each core might take up to 100 GB of RAM in the computation, so keep that in mind when choosing the number of cores!**  

The resulting LOLA databases will be located at `RESULTS/JASPAR_VERSION/GENOME_ASSEMBLY/LOLA_dbs/JASPARVERSION_LOLA_Batch_x.RDS`, where `RESULTS` is the path to the results directory set in the `config.yaml` file.
