configfile: "config.yaml"

DATA = config['DATA_DIR']
BIN = config['BIN']
JASPAR = config['DATA_DIR']
RESULTS = config['RESULTS']

# JASPAR = "{0}/JASPAR2020".format(DATA)

JASPARS, ASSEMBLIES, BATCHES, TFS, BEDS = glob_wildcards(os.path.join(DATA, "{jaspar}", "{assembly}", "TFs", "{batch}", "{tf}", "{bed}.bed"))

rule all:
    input:
        expand(os.path.join(RESULTS, "{jaspar}", "{assembly}", "LOLA_dbs", "JASPAR2020_LOLA_{batch}.RDS"), zip, jaspar = JASPARS, assembly = ASSEMBLIES, batch = BATCHES)


rule collection_and_index:
    input:
        coll = os.path.join(DATA, "{jaspar}", "{assembly}", "collection.txt"),
        index = os.path.join(DATA, "{jaspar}", "{assembly}", "TFs", "{batch}", "{tf}", "index.txt")
    output:
        jasparcoll = os.path.join(RESULTS, "{jaspar}", "{assembly}", "data", "{batch}", "{tf}", "collection.txt"),
        index = os.path.join(RESULTS, "{jaspar}", "{assembly}", "data", "{batch}", "{tf}", "index.txt")
    shell:
        """
        mkdir -p $(dirname {output.jasparcoll})/regions;
        cp {input.coll} {output.jasparcoll};
        cp {input.index} {output.index};
        """

rule beds:
    input:
        index = os.path.join(RESULTS, "{jaspar}", "{assembly}", "data", "{batch}", "{tf}", "index.txt"),
        bed = os.path.join(DATA, "{jaspar}", "{assembly}", "TFs", "{batch}", "{tf}", "{bed}.bed")
    output:
        jasparbed = os.path.join(RESULTS, "{jaspar}", "{assembly}", "data", "{batch}", "{tf}", "regions", "{bed}.bed")
    shell:
        """
        cp {input.bed} {output.jasparbed};
        """

rule createdb:
    input:
        files = expand(os.path.join(RESULTS, "{jaspar}", "{assembly}", "data", "{batch}", "{tf}", "regions", "{bed}.bed"), zip, jaspar = JASPARS, assembly = ASSEMBLIES, batch = BATCHES, tf = TFS, bed = BEDS)
    output:
        os.path.join(RESULTS, "{jaspar}", "{assembly}", "LOLA_dbs", "JASPAR2020_LOLA_{batch}.RDS")
    shell:
        """
        R --vanilla --slave --silent -f {BIN}/create_lola_db.R \
        --args {RESULTS}/{wildcards.jaspar}/{wildcards.assembly}/data/{wildcards.batch} \
        {output};
        """
